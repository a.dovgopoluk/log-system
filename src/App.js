import React from 'react';
import './App.css';
import Navbar from "./default/navbar/Navbar"
import Container from "./default/page/container/Container";
import {BrowserRouter} from "react-router-dom";

function App() {

    return (
        <BrowserRouter>
            <div className="container-fluid">
                <div className="row">
                        <Navbar/>
                    <div className="col-md-9 col-lg-10">
                        <Container/>
                    </div>
                </div>
            </div>
        </BrowserRouter>
  );
}

export default App;
