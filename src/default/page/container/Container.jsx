import React from 'react';
import Main from "../main/Main"
import GraphicsContainer from "../Graphics/GraphicsContainer";
import {GraphicsHead, MainHead, WarningsHead, ValuesHead, BarsHead} from "../../../consts";
import Warnings from "../Warnings/Warnings";
import Values from "../Values/Values"
import BarsContainer from "../Bars/BarsContainer";
import {
    Route,
    Switch,
    Redirect
} from "react-router-dom"

//основной контент страницы

const Container = props =>{
    return (
            <React.Fragment>
                <div className="row">
                    <div className="col-md-12 text-center p-2">
                        {/*Выбор нужного хедера из списка*/}
                        <Switch>
                            <Route path='/main' component={MainHead}/>
                            <Route path='/graphs' component={GraphicsHead}/>
                            <Route path='/warnings' component={WarningsHead}/>
                            <Route path='/values' component={ValuesHead}/>
                            <Route path='/bars' component={BarsHead}/>
                            <Redirect from='/' to='/main'/>
                        </Switch>
                    </div>
                </div>
                <Switch>
                    {/*Выбор нужной страницы из списка*/}
                    <Route path='/main' component={Main}/>
                    <Route path='/graphs' component={GraphicsContainer}/>
                    <Route path='/warnings' component={Warnings}/>
                    <Route path='/values' component={Values}/>
                    <Route path='/bars' component={BarsContainer}/>
                    <Redirect from='/' to='/main'/>
                </Switch>
            </React.Fragment>
    );
}

export default Container;