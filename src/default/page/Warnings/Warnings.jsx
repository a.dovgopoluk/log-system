import React, {Component} from 'react';
import axios from 'axios'
import InputFormWarnings from "./InputFormWarnings";
import Table from "./Table"
import {getFullDT} from "../../../consts"
// import LastValues from "./LastValues";

//страничка предупреждений

class Warnings extends Component  {
    constructor() {
        super();
        var unit = 1;
        var dateStart = new Date(2020, 5, 14);
        var dateEnd = new Date(2020, 5, 14);
        this.state = {
            data: [],
            params: [{
                unit: '-1'
            }],
            options: []
        }
    }

    componentDidMount() {
        this.takeUnits()
    }

    //выбор всех UPS из бд
    takeUnits() {
        return axios.post(`/warnings`, {options: 1})
            .then(res => {
                console.log(res.data);
                this.setState({options: res.data});
                console.log(this.state.options);
                return this.state.options;
            });
    }

    //преобразование даты в нужный формат
    transformDate(input) {
        input.forEach(item=>{
            item.date = getFullDT(+item.date);
            // console.log(item.date)
        })
    }

    //Изменяет предупреждение там, где оно исправлено
    changeGoodValues(data){
        data.forEach(item=>{
            if (+item.flag === 0) item.warning = "Исправлено: " + item.warning;
        })
    }

    //запрос на выбор лога предупреждений
    submit = () => {
        // alert(this.unit);
        // alert(this.dateStart);
        // alert(this.dateEnd);

        const start = this.dateStart.getDate()+"/"+(this.dateStart.getMonth()+1)+"/"+this.dateStart.getFullYear();
        const end = this.dateEnd.getDate()+"/"+(this.dateEnd.getMonth()+1)+"/"+this.dateEnd.getFullYear();
        const unit = this.unit;

        axios.post(`/warnings`, {unit, start, end})
            .then(res => {
                // console.log(res.data);

                this.transformDate(res.data.log);
                this.changeGoodValues(res.data.log);

                this.setState({data: res.data.log});
                this.setState({params: res.data.params});
            });
    }

    setUnit = value =>{
        // alert(value);
        this.unit = value;
        // this.setState({unit: value});
    }

    setDateStart = value =>{
        // alert(value);
        this.dateStart = value;
        // this.setState({dateStart: value});
    }

    setDateEnd = value =>{
        // alert(value);
        this.dateEnd = value;
        // this.setState({dateEnd: value});
    }

    render() {
        return(
            <div className="row">
                <div className="col-md-12 col-lg-4 text-center">
                    <InputFormWarnings setUnit={this.setUnit} setDateStart={this.setDateStart} setDateEnd={this.setDateEnd} submit={this.submit} options={this.state.options}/>
                    {/*<LastValues params={this.state.params}/>*/}
                </div>
                <div className="col-md-12 col-lg-8">
                    <Table data={this.state.data}/>
                </div>
            </div>
    )};


}

export default Warnings