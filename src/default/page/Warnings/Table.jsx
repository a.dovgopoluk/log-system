import React, {Component} from 'react'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table'
import "./Table.css"

//настройки для таблицы
const options ={
    sizePerPageList: [ 5, 10, 15, 20 ],
    sizePerPage: 10,
    page: 1,
    sortName: 'date'
}

// компонент таблицы на странице
class Table extends Component {
    constructor(props) {
        super(props);
    }

    // определение класса для строки (чтобы применить стиль)
    rowClassNameFormat(row, rowIdx) {
        if (row['flag'] === 0) {
            return 'good-row';
        }
        if (row['level'] === 2) return 'err-row';
        if (row['level'] === 1) return 'war-row';
        return 'blank-row';
    }

    render() {
        return (
            <div id="table">
                <BootstrapTable data={this.props.data}
                                trClassName={this.rowClassNameFormat}
                                pagination={ true }
                                options={options}
                >
                    <TableHeaderColumn isKey
                                       dataField='unit'
                                       dataAlign='center'
                                       headerAlign="center"
                                       width="25%"
                    >
                        UPS
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField='date'
                                       dataAlign='center'
                                       headerAlign="center"
                                       width="115px"
                                       minwidth = "115px"
                    >
                        Дата
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField='warning'
                                       dataAlign='center'
                                       headerAlign="center"
                    >
                        Предупреждение
                    </TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default Table