import React, {Component} from 'react';
import Select from 'react-select'
import 'react-day-picker/lib/style.css';
import "./InputFormWarnings.css";
import Period from "../Period/Period";

const opt2 = [
    {value: 1, label: 'lb1'},
    {value: 2, label: 'lb2'},
    {value: 3, label: 'lb3'}
]

class InputFormWarnings extends Component {
    constructor(props) {
        super(props);
        var start = new Date(2020, 5, 14)
        var end = new Date();
        var unit = '';
        this.state = {
            disabled: true,
            other: false,
            otherStyle: {display: 'none'}
        };
    }

    submit = event =>{
        this.props.setUnit(this.unit);
        this.props.setDateStart(this.start);
        var dt = new Date(this.end);
        dt.setDate(dt.getDate() + 1);
        this.props.setDateEnd(dt)
        this.props.submit();
    }

    // изменение UPSа
    change = e => {
        this.setState({disabled: false});
        this.unit = e.value
    }

    setStart = value =>{
        this.start = value;
    }

    setEnd = value =>{
        this.end = value;
    }

    render() {
        return(
            <div className="check">
                <div className="row">
                    <div className="col-md-12 text-center">
                        <Select
                            className = "selector"
                            placeholder = "Выберите ИПБ"
                            onChange={e => this.change(e)}
                            options = {this.props.options}
                        />
                        <hr/>
                    </div>
                </div>
                <Period
                    setStart={this.setStart}
                    setEnd={this.setEnd}
                    disabled={this.state.disabled}
                    submit={this.submit}
                />
            </div>
        )
    }
}
export default InputFormWarnings;