import React, {Component, useState} from 'react';
import InputFormGraphics from "./InputFormGraphics";
import Graphic from "./Graphic";
import axios from "axios";

class GraphicsContainer extends Component{
    constructor(props) {
        super(props);
        this.state = {
            data:[],
            dataKey: ""
        }
        this.changeGraph = this.changeGraph.bind(this);
    }

    // получение данных для отображения
    changeGraph(UPS, graphID, start, end) {
        //запрос
        axios.post(`/graphics`, {UPS, graphID, start, end})
            .then(res => {
                console.log(res.data);
                this.setState({data: res.data});
            });
    }

    render(){
        return (
            <div className="row">
                <div className="col-md-12 col-lg-4 text-center">
                    {<InputFormGraphics changeGraph={this.changeGraph} />}
                </div>
                <div className="col-md-12 col-lg-8 text-center" id="graph">
                    {<Graphic data={this.state.data}/>}
                </div>
            </div>
        );
    }
}

export default GraphicsContainer;