import React, {Component, useState} from 'react';
import Select from 'react-select'
import "./InputFormGraphics.css"
import axios from "axios";
import Period from "../Period/Period";

class InputFormGraphics extends Component{
    constructor(props) {
        super(props);
        var UPS;
        var graph;
        var start = new Date(2020, 6, 15)
        var end = new Date();
        this.state = {
            disabled: true,
            data: [
                { value: "1", label: "UPS 1" },
                { value: "2", label: "UPS 2" },
                { value: "3", label: "UPS 3" }
            ],
            graphNames: [
                { value: "1", label: "Температура" }
            ]
        }
    }

    // передача данных для запроса
    submit = () =>{
        this.props.changeGraph(this.UPS, this.graph, this.start, this.end);
    }

    // изменение даты начала выборки
    setStart = value =>{
        this.start = value;
    }

    // изменение даты конца выборки
    setEnd = value =>{
        this.end = value;
    }

    // при закгрузке компонента делаем запросы
    componentDidMount() {
        axios.post(`/graphics`, {options: 1})
            .then(res => {
                console.log(res.data);

                this.setState({data: res.data});
            });
        axios.post(`/graphics`, {options: 2})
            .then(res => {
                console.log(res.data);

                this.setState({graphNames: res.data});
            });
    }

    // изменение id UPS
    changeUPS = e => {
        this.UPS = e.value
    }

    // изменение id Графика
    changeGraph = e => {
        this.graph = e.value;
        this.setState({disabled: false})
    }

    render() {
        return(
            <div className="check">
                <div className="row">
                    <div className="col-md-12 text-center">
                        <Select
                            placeholder = "Выберите ИПБ"
                            onChange={this.changeUPS}
                            options = {this.state.data}
                        />
                    </div>
                    <div style={{marginTop: 15}} className="col-md-12 text-center">
                        <Select
                            placeholder = "Выберите график"
                            onChange={this.changeGraph}
                            options = {this.state.graphNames}
                        />
                    </div>
                    <div className="col-md-12">
                        <hr/>
                    </div>
                </div>
                    <Period
                        setStart={this.setStart}
                        setEnd={this.setEnd}
                        disabled={this.state.disabled}
                        submit={this.submit}
                    />
            </div>
        )
    }

}
export default InputFormGraphics;