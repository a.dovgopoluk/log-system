import React, {Component} from 'react';
import {getFullDT} from "../../../consts"
import {ScatterChart, Scatter, ResponsiveContainer, LineChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Line} from "recharts";

// кастомизированный класс "лейбочки" при на ведении на точку
class CustomizedAxisTick extends React.PureComponent {
    render() {
        const {
            x, y, stroke, payload,
        } = this.props;

        const date = new Date(payload.value);
        let toShow;
        if (date.getUTCDate().toString().length === 1) toShow = "0" + date.getUTCDate() + ".";
        else toShow = date.getUTCDate() + ".";
        if ((date.getUTCMonth()+1).toString().length === 1) toShow += "0" + (date.getUTCMonth() + 1);
        else toShow += (date.getUTCMonth() + 1);

        return (
            <g transform={`translate(${x},${y})`}>
                <text x={0} y={0} dy={16} fill="#666">{toShow}</text>
            </g>
        );
    }
}

// график с точками
class Graphic extends Component{
    constructor(props) {
        super(props);
    }
    render() {
        var max = 5;
        var min = 20;

        var maxData = 0;
        var minData = 1608261057944000;

        // определяем промежуток
        this.props.data.forEach(item => {
            if (item.Значение > max){
                max = item.Значение;
            }
            if (item.Дата > maxData){
                maxData = item.Дата;
            }
            if (item.Дата < min){
                minData = item.Дата;
            }
            if (item.Значение < minData){
                min = item.Значение;
            }
        })

        return(
                <ResponsiveContainer width='100%' height='100%' minHeight={350}>
                    <ScatterChart
                        margin={{
                            top: 10, right: 20, bottom: 5, left: 0,
                        }}>
                        <CartesianGrid />
                        <XAxis type="number" dataKey="Дата" domain={[minData, maxData+((maxData-minData)*0.07)]} tick={<CustomizedAxisTick/>}/>
                        <YAxis type="number" dataKey="Значение" domain={[min-1, max+2]}/>
                        <Tooltip
                            formatter={(value, name) => {
                                if (name === "Дата"){
                                    return getFullDT(value);
                                }
                                else return value;
                        }}
                            cursor={{ strokeDasharray: '3 3' }} />
                        <Legend />
                        <Scatter data={this.props.data} fill="#8884d8" line legendType = "none" />
                    </ScatterChart>
                </ResponsiveContainer>
        )
    }
}

export default Graphic;