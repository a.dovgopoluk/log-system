import React, {Component} from 'react';
import './itemUPS.css';
import arrow from './arrow.svg';

// контейнер для значений UPS
class ItemUPS extends Component  {
    constructor(props) {
        super(props);
        this.state = {
            arrowStyle: {
                margin: '5px 5px 5px 10px',
                transform: 'rotate(-90deg)'
            },
            other: true,
            otherText: "скрыть",
            otherStyle: {display: 'block'},
        }
    }

    // скрыть/показать остальные значения
    showOther = () =>{
        const temp = this.state.other;
        this.setState({other: !temp});

        if (!temp){
            this.setState({otherStyle: {display: 'block'}});
            this.setState({arrowStyle: {
                    margin: '5px 5px 5px 10px',
                    transform: 'rotate(-90deg)'}});
            this.setState({otherText: "скрыть"});
        }
        else{
            this.setState({otherStyle: {display: 'none'}});
            this.setState({arrowStyle: {
                    margin: '5px 5px 5px 10px',
                    transform: 'rotate(90deg)'}});
            this.setState({otherText: "показать"});
        }
    }

    render() {
        var mainValues = [];
        var otherValues = [];

        for (let i = 0; i < this.props.cvalues.length; i++){
            if(i < 2) {
                mainValues.push(
                    <div className="ups-value">
                        {this.props.cvalues[i].description}: {this.props.cvalues[i].value} {this.props.cvalues[i].unit}
                    </div>
                ) }
            else {
                otherValues.push(
                    <div className="ups-value">
                        {this.props.cvalues[i].description}: {this.props.cvalues[i].value} {this.props.cvalues[i].unit}
                    </div>
                ) }
        }

        let UPShead = "ups-head "; // стиль для отображения названия UPS

        // выбор класса в зависимости от статуса
        switch (this.props.status) {
            case 0:{
                UPShead += "unknown"
                break;
            }
            case 1:{
                UPShead += "good"
                break;
            }
            case 2:{
                UPShead += "check"
                break;
            }
            case 3:{
                UPShead += "bad"
                break;
            }
        }

        return(
                <div className="col-lg-3 col-md-4 col-sm-6">
                    <div className="ups-container">
                        <div className={UPShead}>
                            <span className="ups-name">
                                {this.props.name}
                            </span>
                        </div>
                        <div className="full-ups-name">
                            {this.props.fullName}
                        </div>
                        <div className="ups-info">
                            <div className="main-container">
                                {mainValues}
                            </div>
                            <div className="other-container" style={this.state.otherStyle}>
                                {otherValues}
                            </div>
                            <div>
                                <span onClick={this.showOther} className="other-head">
                                    <span className="other">{this.state.otherText}</span>
                                    <img className="arrow" src={arrow} style={this.state.arrowStyle} alt="arrow"/>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
        )};
}

export default ItemUPS;