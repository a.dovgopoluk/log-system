import React, {Component} from 'react';
import axios from 'axios'
import './Values.css';
import ItemUPS from "./itemUPS";
import UpdateHeader from "./UpdateHeader";

//страничка предупреждений
class Values extends Component  {
    constructor() {
        super();
        this.UPS = {};
        this.status = {};
        this.state = {
            // UPS: {},
            items: []
        }
        this.takeUnits()
            .then(res => this.takeStatus()
                .then(res => this.create()
            )
        )
    }

    // выбор всех UPS из бд
    takeUnits() {
        return axios.post(`/values`, {options: 1})
            .then(res => {
                console.log(res.data);
                this.UPS = res.data;
            });
    }

    // выбор всех статусов
    takeStatus() {
        return axios.post(`/values`, {options: 2})
            .then(res => {
                console.log(res.data);
                this.status = res.data;
            });
    }

    create(){
        var temp = [];
        var names = Object.getOwnPropertyNames(this.UPS);
        for (let i = 0; i < names.length; i++){
            temp.push(<ItemUPS name={names[i]} cvalues={this.UPS[names[i]]} status={this.status[names[i]].status} fullName={this.status[names[i]].fullName}/>)
        }
        this.setState({items: temp});
    }

    render() {
        return(
            <React.Fragment>
                <div className="row">
                    <UpdateHeader/>
                </div>
                <div className="row">
                    {this.state.items}
                    {/*<ItemUPS name={"0"} cvalues={cvalues.UPS1} status={0} fullName={"00"}/>*/}
                </div>
            </React.Fragment>
    )};
}

export default Values