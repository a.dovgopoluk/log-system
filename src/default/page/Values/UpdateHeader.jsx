import React, {Component} from 'react';
import axios from 'axios'
import "./UpdateHeader.css"
import {getFullDT} from "../../../consts"

class UpdateHeader extends Component {
    constructor() {
        super();
        this.DT= "";
        this.takeDT();
    }

    takeDT(){
        return axios.post(`/values`, {options: 3})
            .then(res => {
                // console.log(res.data);
                console.log(res.data.DT);
                this.DT=getFullDT(res.data.DT);
            });
    }

    render() {
        // this.takeDT(); //!!!
        // const date = getFullDT(this.DT);

        return(
            <div className="col-md-12">
                <div id="update">Дата и время последнего обновления: <span>{this.DT}</span></div>
            </div>
        )
    }
}

export default UpdateHeader;