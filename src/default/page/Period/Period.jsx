import React, {Component} from 'react';
import DayPickerInput from "react-day-picker/DayPickerInput";
import dateFnsFormat from 'date-fns/format';
import dateFnsParse from 'date-fns/parse';
import { DateUtils } from "react-day-picker";
import 'react-day-picker/lib/style.css';
import { Button } from 'react-bootstrap';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";

const FORMAT = 'dd/MM/yyyy'; //формат даты

// стиль кнопки
const buttonStyle ={
    margin: "15px 10px 15px 0px",
    font:""
}

// компонент по выбору даты и времени
class Period extends Component {
    constructor(props) {
        super(props);
        var start = new Date(2020, 5, 14)
        var end = new Date();
        this.state = {
            other: false,
            otherStyle: {display: 'none'}
        };

    }

    // отправка значений
    submit = event =>{
        this.props.setStart(this.start);
        var dt = new Date(this.end);
        dt.setDate(dt.getDate() + 1);
        this.props.setEnd(dt);
        this.props.submit();
    }


    //определение формата даты для блоков выбора
    formatDate(date, format, locale) {
        return dateFnsFormat(date, format, { locale });
    }

    //приведение даты к нужному формату
    parseDate(str, format, locale) {
        const parsed = dateFnsParse(str, format, new Date(), { locale });
        if (DateUtils.isDate(parsed)) {
            return parsed;
        }
        return undefined;
    }

    //изменение начальной даты
    changeStart = day => {
        this.start = day;
    }

    //изменение конечной даты
    changeEnd = day => {
        this.end = day;
    }

    switchAnother = (event) => {
        const temp = this.state.other;
        this.setState({other: !temp});

        if (!temp) this.setState({otherStyle: {display: 'block'}});
        else this.setState({otherStyle: {display: 'none'}});
    };

    // установка и отправка текущего дня
    day = () =>{
        this.start = new Date();
        this.end = new Date();
        this.submit();
    }

    // установка и отправка даты со "вчера" по "сегодня"
    yesterday = () =>{
        var dt = new Date();
        dt.setDate(dt.getDate() - 1);
        this.start = dt;
        this.end = dt;
        this.submit();
    }

    // установка и отправка периода в 1 месяц от сегодняшнего дня
    month = () =>{
        var dt = new Date();
        dt.setMonth(dt.getMonth() - 1);
        this.start = dt;
        this.end = new Date();

        this.submit();
    }

    // установка и отправка периода в 1 неделю от сегодняшнего дня
    week = () =>{
        var dt = new Date();
        dt.setDate(dt.getDate() - 7);
        this.start = dt;
        this.end = new Date();
        this.submit();
    }

    render() {
        return(
            <div className="row">
                <div className="col-md-12 text-center" id="period">Выберите период</div>

                <div className="col-md-12">
                    <Button style={buttonStyle} variant="secondary" size="sm" disabled={this.props.disabled} onClick={this.day}>Сегодня</Button>
                    <Button style={buttonStyle} variant="secondary" size="sm" disabled={this.props.disabled} onClick={this.yesterday}>Вчера</Button>
                    <Button style={buttonStyle} variant="secondary" size="sm" disabled={this.props.disabled} onClick={this.week}>Неделя</Button>
                    <Button style={buttonStyle} variant="secondary" size="sm" disabled={this.props.disabled} onClick={this.month}>Месяц</Button>
                </div>

                <div className="col-md-12">
                    <FormControlLabel className = "switch"
                                      control={
                                          <Switch
                                              checked={this.state.other}
                                              onChange={this.switchAnother}
                                              color="primary"
                                              name="another"
                                          />
                                      }
                                      label = "Другой"
                    />
                </div>

                <div className="col-md-6" style={this.state.otherStyle}>
                    <div className="dp">
                        <span className="txt">Начало:</span>
                        <div className="dp-form">
                            <DayPickerInput
                                formatDate={this.formatDate}
                                format={FORMAT}
                                parseDate={this.parseDate}
                                placeholder={`${dateFnsFormat(new Date(), FORMAT)}`}
                                value={this.start}
                                onDayChange={this.changeStart}
                                inputProps={{ style: { width: "100%" } }}
                            />
                        </div>
                    </div>
                </div>
                <div className="col-md-6" style={this.state.otherStyle}>
                    <div className="dp">
                        <span className="txt">Конец:</span>
                        <div className="dp-form">
                            <DayPickerInput
                                formatDate={this.formatDate}
                                format={FORMAT}
                                parseDate={this.parseDate}
                                placeholder={`${dateFnsFormat(new Date(), FORMAT)}`}
                                value={this.end}
                                onDayChange={this.changeEnd}
                                inputProps={{ style: { width: "100%" } }}
                            />
                        </div>
                    </div>
                </div>
                <div className="col-md-12" style={this.state.otherStyle}>
                    <Button style={buttonStyle} variant="secondary" disabled={this.props.disabled} onClick={this.submit}>Получить</Button>
                </div>
                <div className="col-md-12">
                    <hr/>
                </div>
            </div>
        )
    }
}

export default Period;