import React, {Component, useState} from 'react';
import Bars from "./Bars";
import axios from "axios";

// константы показывающие формат входящих значений
const data = [
    {
        "name": "UPS 1",
        "year": 4000,
        "quarter": 2400,
        "month": 1000
    },
    {
        "name": "UPS 2",
        "year": 5000,
        "quarter": 200,
        "month": 100
    },
    {
        "name": "UPS 3",
        "year": 4100,
        "quarter": 2800,
        "month": 800
    },
]

class BarsContainer extends Component{
    constructor(props) {
        super(props);
        this.state = {
            data:[]
        }
        this.getSummary();
    }

    // получение данных 365/91/30
    getSummary() {
        // запрос
        axios.post(`/summary`, {})
            .then(res => {
                console.log(res.data);
                this.setState({data: res.data});
            });
    }

    render(){
        return (
            <div className="row">
                <div className="col-md-12 text-center" id="bars">
                    <Bars data={this.state.data}/>
                </div>
            </div>
        );
    }


}

export default BarsContainer;