import React, {Component} from 'react';
import {CartesianGrid, Legend, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import BarChart from "recharts/lib/chart/BarChart";
import Bar from "recharts/lib/cartesian/Bar";

class Bars extends Component{
    constructor() {
        super();
    }

    // отрисовка графика
    render() {
        return(
            <ResponsiveContainer width='100%' height={350}>
                <BarChart data={this.props.data}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="year" fill="#4472c4" name={"Год"}/>
                    <Bar dataKey="quarter" fill="#ed7d31" name={"Квартал"}/>
                    <Bar dataKey="month" fill="#a5a5a5" name={"Месяц"}/>
                </BarChart>
            </ResponsiveContainer>
        )
    }
}

export default Bars;