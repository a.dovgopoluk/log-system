import React from 'react';
import './Main.css';

function Main() {
    return (
        <div className="row">
            <div className="col-md-7 text-center">
                <img src={require('./startIMG.jpg')} id="startIMG"/>
            </div>
            <div className="col-md-5">
                Вступительная информация
            </div>
        </div>
    );
}

export default Main;