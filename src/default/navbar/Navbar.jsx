import React from "react";
import './Navbar.css';
import ItemRef from "./menuItems/ItemRef";

//меню

const Navbar = props =>{

    return(
        <div className="col-md-3 col-lg-2 navbar-container bg-light">

                    <nav className="navbar navbar-expand-md navbar-light">
                        <img src={require('./mainLogo.jpg')} id="logo"/>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar">
                            <span className="navbar-toggler-icon"/>
                        </button>
                        <div className="collapse navbar-collapse" id="navbar">
                            <ul className="navbar-nav nav-tabs mr-auto mb-auto mt-2">

                                <ItemRef name="Графики" toRef="/graphs"/>
                                <ItemRef name="Предупреждения" toRef="/warnings"/>
                                <ItemRef name="Значения" toRef="/values"/>
                                <ItemRef name="Сводный график" toRef="/bars"/>
                                <ItemRef name="Информация" toRef="/"/>
                            </ul>
                        </div>
                    </nav>

        </div>
    )
}

export default Navbar;
