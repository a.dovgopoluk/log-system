import React from "react";
import {NavLink} from "react-router-dom";

//пункт меню

const ItemRef = props => {
    const name = props.name //текст
    const toRef = props.toRef //ссылка

    return(
        <li className="nav-item">
            <NavLink className="nav-link" to={toRef}>{name}</NavLink>
        </li>
    )
}

export default ItemRef;