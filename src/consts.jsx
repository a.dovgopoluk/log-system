import React from 'react';

//константные хедеры для страниц

const MainHead = () =>(
    <div>
        <h1>Главная страница</h1>
        <hr/>
    </div>
)

const GraphicsHead = () =>(
    <div>
        <h1>Отображение графиков</h1>
        <hr/>
    </div>
)

const WarningsHead = () =>(
    <div>
        <h1>Предупреждения и ошибки</h1>
        <hr/>
    </div>
)

const ValuesHead = () =>(
    <div>
        <h1>Значения</h1>
        <hr/>
    </div>
)

const BarsHead = () =>(
    <div>
        <h1>Сводный график</h1>
        <hr/>
    </div>
)

// const getFullDT = (value) => {
//     // alert(value);
//     const date = new Date(+value);
//
//     let toShow;
//     if (date.getDate().toString().length === 1) toShow = "0" + date.getDate() + ".";
//     else toShow = date.getDate() + ".";
//     if ((date.getMonth()+1).toString().length === 1) toShow += "0" + (date.getMonth() + 1);
//     else toShow += (date.getMonth() + 1);
//
//     toShow += "." + date.getFullYear() + " ";
//
//     if (date.getHours().toString().length === 1) toShow += "0" + date.getHours() + ":";
//     else toShow += date.getHours() + ":";
//     if (date.getMinutes().toString().length === 1) toShow += "0" + date.getMinutes();
//     else toShow += date.getMinutes();
//
//     // alert(toShow);
//     return toShow;
// }

const getFullDT = (value) => {
    // alert(value);
    const date = new Date(+value);

    let toShow;
    if (date.getUTCDate().toString().length === 1) toShow = "0" + date.getUTCDate() + ".";
    else toShow = date.getUTCDate() + ".";
    if ((date.getUTCMonth()+1).toString().length === 1) toShow += "0" + (date.getUTCMonth() + 1);
    else toShow += (date.getUTCMonth() + 1);

    toShow += "." + date.getFullYear() + " ";

    if (date.getUTCHours().toString().length === 1) toShow += "0" + date.getUTCHours() + ":";
    else toShow += date.getUTCHours() + ":";
    if (date.getUTCMinutes().toString().length === 1) toShow += "0" + date.getUTCMinutes();
    else toShow += date.getUTCMinutes();

    // alert(toShow);
    return toShow;
}

export {MainHead, GraphicsHead, WarningsHead, ValuesHead, BarsHead, getFullDT}